//
//  ConnectionAPI.h
//  CodingChallenge1
//
//  Created by Bryan Ruiz on 11/26/19.
//  Copyright © 2019 Bryan Ruiz. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^SuccessBlock) (NSData *response);
typedef void(^FailureBlock) (NSError *error);

@interface ConnectionAPI : NSObject

@property (nonatomic, strong) SuccessBlock successBlock;
@property (nonatomic, strong) FailureBlock failureBlock;
@property (nonatomic, strong) NSURL *requestURL;
@property (nonatomic, strong) NSMutableData *responseData;

- (id)initWithURL:(NSURL *)requestURL successBlock:(SuccessBlock)successBlock failureBlock:(FailureBlock)failureBlock;
- (void)startRequest;

@end
