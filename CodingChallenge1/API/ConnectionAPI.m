//
//  ConnectionAPI.m
//  CodingChallenge1
//
//  Created by Bryan Ruiz on 11/26/19.
//  Copyright © 2019 Bryan Ruiz. All rights reserved.
//

#import "ConnectionAPI.h"

@implementation ConnectionAPI

// MARK: - Variables

@synthesize successBlock = _successBlock;
@synthesize failureBlock = _failureBlock;
@synthesize requestURL = _requestURL;
@synthesize responseData = _responseData;

// MARK: - Constructor

- (id)initWithURL:(NSURL *)requestURL successBlock:(SuccessBlock)successBlock failureBlock:(FailureBlock)failureBlock {
    self = [super init];
    if (self) {
        self.requestURL = requestURL;
        self.successBlock = successBlock;
        self.failureBlock = failureBlock;
        self.responseData = [[NSMutableData alloc] init];
    }
    return self;
}

// MARK: - Get method

- (void)startRequest {
    ///got this code from https://stackoverflow.com/questions/40016361/nsurlsession-request-and-response
    NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] initWithURL:self.requestURL];
    [urlRequest setHTTPMethod:@"GET"];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
    {
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
        if (httpResponse.statusCode == 200) {
          [self.responseData setLength:0];
          [self.responseData appendData:data];
          self.successBlock([NSData dataWithData:self.responseData]);
        }
        else {
          self.responseData = nil;
          self.failureBlock(error);
        }
    }];
    [dataTask resume];
}

@end
