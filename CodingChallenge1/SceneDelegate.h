//
//  SceneDelegate.h
//  CodingChallenge1
//
//  Created by Novacomp on 11/25/19.
//  Copyright © 2019 Novacomp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

