//
//  BlogCell.h
//  CodingChallenge1
//
//  Created by Bryan Ruiz on 11/26/19.
//  Copyright © 2019 Bryan Ruiz. All rights reserved.
//
#import <UIKit/UIKit.h>

@interface BlogCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *blogImageView;
@property (nonatomic, weak) IBOutlet UILabel *title;
@property (nonatomic, weak) IBOutlet UILabel *description;
@property (nonatomic, weak) IBOutlet UILabel *author;
@property (nonatomic, weak) IBOutlet UILabel *article_date;

@end
