//
//  BlogCell.m
//  CodingChallenge1
//
//  Created by Bryan Ruiz on 11/26/19.
//  Copyright © 2019 Bryan Ruiz. All rights reserved.
//

#import "BlogCell.h"

@implementation BlogCell

// MARK: - Components

@synthesize blogImageView = _blogImageView;
@synthesize title = _title;
@synthesize description = _description;
@synthesize author = _author;
@synthesize article_date = _article_date;

@end
