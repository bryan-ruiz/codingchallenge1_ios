//
//  BlogViewController.h
//  CodingChallenge1
//
//  Created by Bryan Ruiz on 11/25/19.
//  Copyright © 2019 Bryan Ruiz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "ConnectionAPI.h"
#import "Article.h"
#import "BlogCell.h"
#import "LinkDetailViewController.h"

@interface BlogViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, copy) NSMutableArray *articles;
@property (nonatomic, weak) IBOutlet UITableView *tableView;

- (void)setResponseCallBacks:(NSURL *)url;
- (void)prepareResponseUrl;
- (NSMutableArray *)setArticlesFromJson:(NSDictionary *)jsonDictionary;

@end
