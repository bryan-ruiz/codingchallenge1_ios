//
//  BlogViewController.m
//  CodingChallenge1
//
//  Created by Bryan Ruiz on 11/25/19.
//  Copyright © 2019 Bryan Ruiz. All rights reserved.
//

#import "BlogViewController.h"
#import "ErrorAlert.h"
#import "String.h"

@implementation BlogViewController

// MARK: - Variables

@synthesize articles = _articles;
@synthesize tableView = _tableView;

// MARK: - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self prepareResponseUrl];
}

// MARK: - Connection to API

- (void)prepareResponseUrl {
    NSURL *url = [NSURL URLWithString:@"https://www.beenverified.com/articles/index.ios.json"];
    
    [self setResponseCallBacks:url];
}

- (NSMutableArray *)setArticlesFromJson:(NSDictionary *)jsonDictionary {
    NSMutableArray *articlesTemp = [NSMutableArray array];
    for (id article in [jsonDictionary objectForKey:@"articles"]) {
        NSString *title = [article objectForKey:@"title"];
        NSString *description = [article objectForKey:@"description"];
        NSString *author = [article objectForKey:@"author"];
        NSString *image = [article objectForKey:@"image"];
        NSString *article_date = [article objectForKey:@"article_date"];
        NSString *link = [article objectForKey:@"link"];
        NSString *uuid = [article objectForKey:@"uuid"];
        [articlesTemp addObject:[Article articleWithTitle:title description:description author:author image:image article_date:article_date link:link uuid:uuid]];
    }
    return articlesTemp;
}

- (void)setResponseCallBacks:(NSURL *)url {
    SuccessBlock successBlock = ^(NSData *response) {
        NSError *error;
        NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:response options:0 error:&error];
        if (jsonDictionary) {
            self.articles = [self setArticlesFromJson:jsonDictionary];
            dispatch_sync(dispatch_get_main_queue(), ^{[self.tableView reloadData];});
        }
    };
    FailureBlock failureBlock = ^(NSError *response) {
        [self presentViewController:[[[ErrorAlert alloc] init] showError:@"Error" message:@"Unable to load information. Please try again"]  animated:YES completion:nil];
    };
    ConnectionAPI *connectionAPI = [[ConnectionAPI alloc] initWithURL:url successBlock:successBlock failureBlock:failureBlock];
    [connectionAPI startRequest];
}

// MARK: - Delegates

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    BlogCell *blogCell = [tableView dequeueReusableCellWithIdentifier:@"blogCell"];
    Article *article = [self.articles objectAtIndex:indexPath.row];
    [blogCell.title setAttributedText: [[[String alloc] init] boldTitle:@"Title: " text:article.title]];
    [blogCell.description setAttributedText: [[[String alloc] init] boldTitle:@"Description: " text:article.description]];
    [blogCell.author setAttributedText: [[[String alloc] init] boldTitle:@"Author: " text: article.author]];
    [blogCell.author setAttributedText: [[[String alloc] init] boldTitle:@"Date: " text: [[[String alloc] init] textToSplit:article.article_date divider:@"+" validation:false]]];
    blogCell.blogImageView.image = [UIImage imageWithData: [[NSData alloc] initWithContentsOfURL: [NSURL URLWithString: [[[String alloc] init] textToSplit:article.image divider:@"com/" validation:true]]]];
    return blogCell;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.articles.count;
}

// MARK: - Scenes connection

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showLinkDetail"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        Article *article = [self.articles objectAtIndex:indexPath.row];
        LinkDetailViewController *linkDetailViewController = [segue destinationViewController];
        linkDetailViewController.link = article.link;
    }
}

@end
