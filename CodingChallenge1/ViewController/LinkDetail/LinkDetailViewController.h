//
//  LinkDetail.h
//  CodingChallenge1
//
//  Created by Bryan Ruiz on 11/27/19.
//  Copyright © 2019 Bryan Ruiz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>
#import <Foundation/Foundation.h>

@interface LinkDetailViewController : UIViewController<WKUIDelegate>

@property (weak, nonatomic) IBOutlet WKWebView *webView;
@property (nonatomic, copy) NSString *link;

- (void)showInfo;

@end
