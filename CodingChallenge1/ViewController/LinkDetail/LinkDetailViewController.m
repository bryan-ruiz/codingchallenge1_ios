//
//  LinkDetail.m
//  CodingChallenge1
//
//  Created by Bryan Ruiz on 11/27/19.
//  Copyright © 2019 Bryan Ruiz. All rights reserved.
//

#import "LinkDetailViewController.h"

@implementation LinkDetailViewController

// MARK: - Variables

@synthesize link = _link;
@synthesize webView = _webView;

// MARK: - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self showInfo];
}

// MARK: - Webview config

- (void)showInfo {
    self.webView.UIDelegate = self;
    NSURL *url=[NSURL URLWithString:self.link];
    NSURLRequest *redirectToLink=[NSURLRequest requestWithURL:url];
    [self.webView loadRequest:redirectToLink];
}

@end
