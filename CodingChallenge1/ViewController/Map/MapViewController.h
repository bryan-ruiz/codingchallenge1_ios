//
//  MapViewController.h
//  CodingChallenge1
//
//  Created by Bryan Ruiz on 11/25/19.
//  Copyright © 2019 Bryan Ruiz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>

@interface MapViewController : UIViewController<MKMapViewDelegate, CLLocationManagerDelegate, UISearchBarDelegate>

@property (nonatomic, weak) IBOutlet MKMapView *mapView;
@property (nonatomic, weak) IBOutlet UISearchBar *searchBar;

@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) CLLocation *location;
//@property (readwrite, nonatomic) MKMapItem *mapItemList;

@property (readwrite, nonatomic) CLLocationCoordinate2D *currentLocation;

- (void)initLocationManager;
- (void)setLocationManager;
- (void)removeAnnotationsFromMap;
- (void)setAnnotationTo:(NSString *)place placemark:(CLPlacemark *)placemark;
- (void)setRegionFor:(CLPlacemark *)placemark;
- (NSArray *)filterSearchResultsUpTo5:(NSArray *)mapItems;
- (void)addPlaceAnnotation:(MKMapItem *)mapItem;

@end

