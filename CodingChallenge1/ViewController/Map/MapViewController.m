//
//  MapViewController.m
//  CodingChallenge1
//
//  Created by Bryan Ruiz on 11/25/19.
//  Copyright © 2019 Bryan Ruiz. All rights reserved.
//

#import "MapViewController.h"
#import "ErrorAlert.h"

@implementation MapViewController

// MARK: - Variables

@synthesize mapView = _mapView;
@synthesize searchBar = _searchBar;
@synthesize location = _location;
@synthesize locationManager = _locationManager;
@synthesize currentLocation = _currentLocation;

// MARK: - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initLocationManager];
    [self setLocationManager];
}

// MARK: - Annotation

- (void)removeAnnotationsFromMap {
    /// got this code from https://stackoverflow.com/questions/10865088/how-do-i-remove-all-annotations-from-mkmapview-except-the-user-location-annotati/14949650
    NSMutableArray * annotationsToRemove = [self.mapView.annotations mutableCopy];
    [annotationsToRemove removeObject:self.mapView.userLocation];
    [self.mapView removeAnnotations:annotationsToRemove];
}

- (void)setRegionFor:(CLPlacemark *)placemark {
    float spanX = 0.00725;
    float spanY = 0.00725;
    MKCoordinateRegion region;
    region.center.latitude = placemark.location.coordinate.latitude;
    region.center.longitude = placemark.location.coordinate.longitude;
    region.span = MKCoordinateSpanMake(spanX, spanY);
    [self.mapView setRegion:region animated:true];
}

- (void)setAnnotationTo:(NSString *)place placemark:(CLPlacemark *)placemark {
    MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
    annotation.coordinate = CLLocationCoordinate2DMake(placemark.location.coordinate.latitude, placemark.location.coordinate.longitude);
    annotation.title = place;
    [self.mapView addAnnotation:annotation];
}

- (void)addPlaceAnnotation:(MKMapItem *)mapItem {
    /// got this code from https://www.devfright.com/clgeocoder-and-clplacemark-tutorial-how-to-find-location-information/
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [geocoder geocodeAddressString:[[mapItem placemark] title] completionHandler:^(NSArray *placemarks, NSError *error) {
        if (error) {
            [self presentViewController:[[[ErrorAlert alloc] init] showError:@"Error" message:@"Place not found. Please try again"]  animated:YES completion:nil];
        } else {
            CLPlacemark *placemark = [placemarks lastObject];
            [self setRegionFor:placemark];
            [self setAnnotationTo:[mapItem name] placemark:placemark];
        }
    }];
}

// MARK: - Map config

- (void)initLocationManager {
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
}

- (void)setLocationManager {
    self.mapView.showsUserLocation = true;
    self.mapView.showsBuildings = true;
    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [self.locationManager requestWhenInUseAuthorization];
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    }
    [self.locationManager startUpdatingLocation];
}

// MARK: - Search place

- (NSArray *)filterSearchResultsUpTo5:(NSArray *)mapItems {
    NSArray *itemsForView = mapItems;
    if (mapItems.count > 5) {
        itemsForView = [mapItems subarrayWithRange: NSMakeRange( 0, 5)];
    }
    return itemsForView;
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self removeAnnotationsFromMap];
    ///got this code from https://stackoverflow.com/questions/13798804/use-mklocalsearch-to-search-for-locations-on-a-map
    MKLocalSearchRequest *searchRequest = [[MKLocalSearchRequest alloc] init];
    [searchRequest setNaturalLanguageQuery:searchBar.text];
    MKLocalSearch *localSearch = [[MKLocalSearch alloc] initWithRequest:searchRequest];
    [localSearch startWithCompletionHandler:^(MKLocalSearchResponse *response, NSError *error) {
        if (!error) {
            for (MKMapItem *mapItem in [self filterSearchResultsUpTo5:[response mapItems]]) {
                [self addPlaceAnnotation:mapItem];
            }
        } else {
            [self presentViewController:[[[ErrorAlert alloc] init] showError:@"Error" message:@"Place not found. Please try again"]  animated:YES completion:nil];
        }
    }];
}

// MARK: - Delegates

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation {
    /// got this code from https://www.youtube.com/watch?v=8_qwwaL9pWI
    MKMapCamera *camera = [MKMapCamera cameraLookingAtCenterCoordinate:userLocation.coordinate fromEyeCoordinate:CLLocationCoordinate2DMake(userLocation.coordinate.latitude, userLocation.coordinate.longitude) eyeAltitude:10000];
    [self.mapView setCamera:camera animated:true];
}

@end
