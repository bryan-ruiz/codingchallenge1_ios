//
//  String.m
//  CodingChallenge1
//
//  Created by Novacomp on 12/2/19.
//  Copyright © 2019 Novacomp. All rights reserved.
//

#import "String.h"

@implementation String

// MARK: - Bold

- (NSMutableAttributedString *)boldTitle:(NSString *)title text:(NSString *)text {
    /// got this code from: https://stackoverflow.com/questions/27222684/how-to-make-part-of-a-string-bold-in-ios
    NSString *fullString = [NSString stringWithFormat:@"%@%@", title, text];
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:fullString];
    NSRange boldRange = [fullString rangeOfString:title];
    [attributedString addAttribute: NSFontAttributeName value:[UIFont boldSystemFontOfSize:17] range:boldRange];
    return attributedString;
}

// MARK: - Split

- (NSString *)textToSplit:(NSString *)text divider:(NSString *)key validation:(Boolean)isImage {
    NSArray *separatedText = [text componentsSeparatedByString:key];
    if (isImage) {
        return [NSString stringWithFormat:@"%@%@%@%@", [separatedText objectAtIndex:0], @"com/", @"fit-in/60x/filters:autojpg()/", [separatedText objectAtIndex:1]];
    }
    else {
        return [NSString stringWithFormat:@"%@", [separatedText objectAtIndex:0]];
    }
}

@end
