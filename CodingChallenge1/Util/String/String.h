//
//  String.h
//  CodingChallenge1
//
//  Created by Novacomp on 12/2/19.
//  Copyright © 2019 Novacomp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface String : NSObject

- (NSMutableAttributedString *)boldTitle:(NSString *)title text:(NSString *)text;
- (NSString *)textToSplit:(NSString *)text divider:(NSString *)key validation:(Boolean)isImage;

@end
