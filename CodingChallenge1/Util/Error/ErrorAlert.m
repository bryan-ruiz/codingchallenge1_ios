//
//  ErrorAlert.m
//  CodingChallenge1
//
//  Created by Novacomp on 11/28/19.
//  Copyright © 2019 Novacomp. All rights reserved.
//

#import "ErrorAlert.h"

@implementation ErrorAlert

// MARK: - Alert dialog

- (UIAlertController *)showError:(NSString *)title message:(NSString *)message {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message: message preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *firstAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:firstAction];
    return alert;
}

@end
