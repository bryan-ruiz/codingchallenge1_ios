//
//  ErrorAlert.h
//  CodingChallenge1
//
//  Created by Novacomp on 11/28/19.
//  Copyright © 2019 Novacomp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ErrorAlert : NSObject

- (UIAlertController *)showError:(NSString *)title message:(NSString *)message;

@end
