//
//  Article.h
//  CodingChallenge1
//
//  Created by Bryan Ruiz on 11/25/19.
//  Copyright © 2019 Bryan Ruiz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Article: NSObject

@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *description;
@property (nonatomic, copy) NSString *author;
@property (nonatomic, copy) NSString *image;
@property (nonatomic, copy) NSString *article_date;
@property (nonatomic, copy) NSString *link;
@property (nonatomic, copy) NSString *uuid;

+ (id)articleWithTitle:(NSString *)title description:(NSString *)description author:(NSString *)author image:(NSString *)image article_date:(NSString *)article_date link:(NSString *)link uuid:(NSString *)uuid;

- (id)initWithTitle:(NSString *)title description:(NSString *)description author:(NSString *)author image:(NSString *)image article_date:(NSString *)article_date link:(NSString *)link uuid:(NSString *)uuid;

@end
