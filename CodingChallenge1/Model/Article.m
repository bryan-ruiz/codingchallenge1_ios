//
//  Article.m
//  CodingChallenge1
//
//  Created by Bryan Ruiz on 11/25/19.
//  Copyright © 2019 Bryan Ruiz. All rights reserved.
//

#import "Article.h"

@implementation Article

// MARK: - Variables

@synthesize title = _title;
@synthesize description = _description;
@synthesize author = _author;
@synthesize image = _image;
@synthesize article_date = _article_date;
@synthesize link = _link;
@synthesize uuid = _uuid;

// MARK: - Constructors

+ (id)articleWithTitle:(NSString *)title description:(NSString *)description author:(NSString *)author image:(NSString *)image article_date:(NSString *)article_date link:(NSString *)link uuid:(NSString *)uuid {
    return [[[self class] alloc] initWithTitle:title description:description author:author image:image article_date:article_date link:link uuid:uuid];
}

- (id)initWithTitle:(NSString *)title description:(NSString *)description author:(NSString *)author image:(NSString *)image article_date:(NSString *)article_date link:(NSString *)link uuid:(NSString *)uuid {
    self = [super init];
    if (self) {
        self.title = title;
        self.description = description;
        self.author = author;
        self.image = image;
        self.article_date = article_date;
        self.link = link;
        self.uuid = uuid;
    }
    return self;
}

@end
